import scrapy
from selenium import webdriver
from time import sleep

from selenium.webdriver import DesiredCapabilities


class ReviewsSpider(scrapy.Spider):
	name = "reviews"

	# start_urls = ["https://www.tripadvisor.com/Restaurant_Review-g60763-d3603515-Reviews-Spice_Symphony-New_York_City_New_York.html"] + \
	# 			 ["https://www.tripadvisor.com/Restaurant_Review-g60763-d3603515-Reviews-or{}-Spice_Symphony-New_York_City_New_York.html".format(page) for page in range(10, 1650, 10)] + \
	#  			   ["https://www.tripadvisor.com/Restaurant_Review-g60763-d4737024-Reviews-5_Napkin_Burger-New_York_City_New_York.html"] + \
	# 			 [ "https://www.tripadvisor.com/Restaurant_Review-g60763-d4737024-Reviews-or{}-5_Napkin_Burger-New_York_City_New_York.html".format(page) for page in range(10, 2280, 10)]

	start_urls = ["https://www.tripadvisor.com/Restaurant_Review-g60763-d4363835-Reviews-Piccola_Cucina_Osteria-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d4363835-Reviews-or{}-Piccola_Cucina_Osteria-New_York_City_New_York.html".format(page) for page in range(10, 840, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d1236281-Reviews-Club_A_Steakhouse-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d1236281-Reviews-or{}-Club_A_Steakhouse-New_York_City_New_York.html".format(page) for page in range(10, 2510, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d457808-Reviews-Daniel-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d457808-Reviews-or{}-Daniel-New_York_City_New_York.html".format(page) for page in range(10, 2410, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d478026-Reviews-Patsy_s_Italian_Restaurant-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d478026-Reviews-or{}-Patsy_s_Italian_Restaurant-New_York_City_New_York.html".format(page) for page in range(10, 3880, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d2165918-Reviews-Patzeria_Family_Friends-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d2165918-Reviews-or{}-Patzeria_Family_Friends-New_York_City_New_York.html".format(page) for page in range(10, 2660, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d783460-Reviews-Levain_Bakery-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d783460-Reviews-or{}-Levain_Bakery-New_York_City_New_York.html".format(page) for page in range(10, 2850, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d4578404-Reviews-Los_Tacos_No_1-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d4578404-Reviews-or{}-Los_Tacos_No_1-New_York_City_New_York.html".format(page) for page in range(10, 1040, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d424434-Reviews-Le_Bernardin-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d424434-Reviews-or{}-Le_Bernardin-New_York_City_New_York.html".format(page) for page in range(10, 2610, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d7617787-Reviews-K_Rico-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d7617787-Reviews-or{}-K_Rico-New_York_City_New_York.html".format(page) for page in range(10, 1910, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d424094-Reviews-Gramercy_Tavern-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d424094-Reviews-or{}-Gramercy_Tavern-New_York_City_New_York.html".format(page) for page in range(10, 3090, 10)] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d478965-Reviews-Gallagher_s_Steak_House-New_York_City_New_York.html"] + \
	["https://www.tripadvisor.com/Restaurant_Review-g60763-d478965-Reviews-or{}-Gallagher_s_Steak_House-New_York_City_New_York.html".format(page) for page in range(10, 2480, 10)]

	def __init__(self):
		# options = webdriver.ChromeOptions()
		# options.add_experimental_option('prefs', {'intl.accept_languages': 'en,en_US'})
		# self.driver = webdriver.Chrome(chrome_options=options, executable_path=r"C:\Users\felipe.zschornack\workspace\reviews_tripadvisor\chromedriver.exe")
		self.driver = webdriver.Firefox(executable_path=r"/Users/fzschornack/mestrado/reviews_tripadvisor/geckodriver")

	def parse(self, response):
		# <span class="taLnk ulBlueLinks" onclick="widgetEvCall('handlers.clickExpand',event,this);">More</span>
		self.driver.get(response.request.url) #"https://www.tripadvisor.com/Restaurant_Review-g60763-d3603515-Reviews-Spice_Symphony-New_York_City_New_York.html")


		wait = True
		while(wait):
			try:
				old_html = self.driver.execute_script("return document.getElementById('taplc_location_reviews_list_0').innerHTML")
				wait = False
			except Exception, e:
				wait = True

		old_html = self.driver.execute_script("return document.getElementsByClassName('ulBlueLinks')[0].innerHTML")
		
		print("#####OLD_HTML",old_html)

		if(old_html != None):
			button_more = self.driver.find_element_by_class_name("ulBlueLinks")
			button_more.click()

			wait = True
			new_html = "anything"
			while(wait or (new_html == old_html)):
				try:
					new_html = self.driver.execute_script("return document.getElementsByClassName('ulBlueLinks')[0].innerHTML")
					wait = False
				except Exception, e:
					wait = True

			new_html = self.driver.execute_script("return document.getElementsByClassName('ulBlueLinks')[0].innerHTML")

			print("#####NEW_HTML",new_html)
		
		wait = True
		while(wait):
			try:
				new_html = self.driver.execute_script("return document.getElementById('taplc_location_reviews_list_0').innerHTML")
				wait = False
			except Exception, e:
				wait = True

		new_html = self.driver.execute_script("return document.getElementById('taplc_location_reviews_list_0').innerHTML")

		response = response.replace(body=new_html.replace('<br>', '\n'))

		for review in response.css('p.partial_entry'):
			yield {
				'review': review.css('p.partial_entry::text').extract_first()
			}

		# next_page = response.xpath('//a[.="Next"]') #.extract_first()
		# print(next_page)
		# for a in next_page:
		# 	yield response.follow(a, callback=self.parse)
